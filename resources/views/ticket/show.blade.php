<!-- Session Status -->
<x-auth-session-status class="mb-4" :status="session('status')" />

{{$ticket}}
<a href="{{route('ticket.show',$ticket->id)}}">{{$ticket->title}}</a>
<p>{{$ticket->description}}</p>
<a href="{{route('ticket.edit',$ticket->id)}}">
    <x-primary-button>Edit</x-primary-button>
</a>
<form action="{{route('ticket.delete',$ticket->id)}}" method="post">
    @csrf
    @method('delete')
    <x-primary-button>Delete</x-primary-button>
</form>
