<!-- Session Status -->
<x-auth-session-status class="mb-4" :status="session('status')" />

<!-- <form method="GET"> -->
<form method="POST" action="{{ route('ticket.update', $ticket->id) }}">
    @csrf
    @method('patch')

    <!-- Title -->
    <div>
        <x-input-label for="title" :value="__('Title')" />
        <x-text-input id="title" class="block mt-1 w-full" type="text" name="title" :value="!empty($ticket->title) ? $ticket->title : old('title')" required autofocus autocomplete="username" />
        <x-input-error :messages="$errors->get('title')" class="mt-2" />
    </div>

    <!-- Description -->
    <div class="mt-4">
        <x-input-label for="description" :value="__('Description')" />

        <textarea id="description" class="block mt-1 w-full"
                        name="description"
                        required autocomplete="description">{{!empty($ticket->description) ? $ticket->description : '' }}
        </textarea>
        <x-input-error :messages="$errors->get('description')" class="mt-2" />
    </div>

    <div class="flex items-center justify-end mt-4">
        <x-primary-button class="ms-3">
            {{ __('Update') }}
        </x-primary-button>
    </div>
</form>
