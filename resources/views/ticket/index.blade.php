<!-- Session Status -->
<x-auth-session-status class="mb-4" :status="session('status')" />

@foreach($tickets as $ticket)
{{$ticket}}
<a href="{{route('ticket.show',$ticket->id)}}">{{$ticket->title}}</a>
<p>{{$ticket->description}}</p>
@endforeach