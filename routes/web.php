<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    // $users = DB::select('select * from users');
    // dd($users);
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::resource('/ticket', TicketController::class);

    Route::get('/create', [TicketController::class, 'create'])->name('ticket.create');
    Route::post('/store', [TicketController::class, 'store'])->name('ticket.store');
    Route::delete('/delete/{id}', [TicketController::class, 'delete'])->name('ticket.delete');
});


// Route::get('/create', function(){
//     return view('ticket.create');
// })->name('ticket.create');

require __DIR__.'/auth.php';
